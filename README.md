# daily

[![CI Status](http://img.shields.io/travis/liamscofield/daily.svg?style=flat)](https://travis-ci.org/liamscofield/daily)
[![Version](https://img.shields.io/cocoapods/v/daily.svg?style=flat)](http://cocoapods.org/pods/daily)
[![License](https://img.shields.io/cocoapods/l/daily.svg?style=flat)](http://cocoapods.org/pods/daily)
[![Platform](https://img.shields.io/cocoapods/p/daily.svg?style=flat)](http://cocoapods.org/pods/daily)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

daily is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "daily"
```

## Author

liamscofield, limitscofield@gmail.com

## License

daily is available under the MIT license. See the LICENSE file for more info.
